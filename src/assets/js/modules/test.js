var testModule = (function () {

    function init() {
        console.log('test module initialised!');
    }

    return {
        init: init
    };

})();

testModule.init();
