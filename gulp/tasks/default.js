var config = require('../config'),
    gulp = require('gulp');

gulp.task('default', ['css', 'global-scripts', 'vendor-scripts', 'img', 'templates'], function(){
    if(!config.isProduction) {
        gulp.start('serve');
    }
});
