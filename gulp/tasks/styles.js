var config = require('../config'),
    gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('css', function() {

    return gulp.src('./app/assets/css/*.scss')
        .pipe(sass({outputStyle: (config.isProduction) ? 'compressed' : 'expanded' }).on('error', sass.logError))
        .pipe(gulp.dest(config.paths.css.output));

});
