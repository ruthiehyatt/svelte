var config = require('../config'),
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('global-scripts', function () {

    return gulp.src(config.paths.js.input)
        .pipe(gulpif(!config.isProduction, sourcemaps.init()))
        .pipe(concat('global.js'))
        .pipe(gulpif(config.isProduction, uglify()))
        .pipe(gulpif(!config.isProduction, sourcemaps.write({addComment: true})))
        .pipe(gulp.dest(config.paths.js.output));

});
