var config = require('../config'),
    gulp = require('gulp'),
    swig = require('gulp-swig');

var swigOptions = {
    setup: swig => {
        swig.setDefaults({
            cache: false,
            autoescape: false,
            varControls: ['{!', '!}']
        })
    }
};

gulp.task('templates', function () {

    return gulp.src(config.paths.html.input)
        .pipe(swig({ defaults: { cache: false } }))
        .pipe(gulp.dest(config.paths.html.output));

});
