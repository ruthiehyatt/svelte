var config = require('../config'),
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('serve', function() {
    browserSync({
        server: './dist/'
    });
    gulp.watch(config.paths.js.input, ['global-js', reload]);
    gulp.watch(config.paths.img.input, ['img', reload]);
    gulp.watch(config.paths.css.input, ['css', reload]);
    gulp.watch(config.paths.html.input, ['templates', reload]);
});
