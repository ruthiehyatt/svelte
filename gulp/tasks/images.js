var config = require('../config'),
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    imagemin = require('gulp-imagemin');

gulp.task('img', function(){
    gulp.src(config.paths.img.input)
        .pipe(gulpif(config.isProduction, imagemin()))
        .pipe(gulp.dest(config.paths.img.output));
});
