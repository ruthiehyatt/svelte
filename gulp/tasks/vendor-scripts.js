var config = require('../config'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('vendor-scripts', function () {

    return gulp.src(config.paths.vendor.input)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.paths.vendor.output));

});
