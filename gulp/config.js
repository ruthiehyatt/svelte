var argv = require('yargs').argv;

module.exports = {
    isProduction: (argv.build === undefined) ? false : true,
    paths: {
        html: {
            input: './src/templates/pages/*.tpl',
            output: './dist/'
        },
        css: {
            input: './src/assets/css/**/*.scss',
            output: './dist/assets/css/'
        },
        img: {
            input: [
                './src/assets/img/**/*.{png, jpeg, jpg}',
                '!./src/assets/img/icons/*.png'
            ],
            output: './dist/assets/img/' 
        },
        js: {
            input: './src/assets/js/modules/*.js',
            output: './dist/assets/js/'
        },
        vendor: {
            input: [
                './app/assets/js/vendor/jquery/dist/jquery.js'
            ],
            output: './dist/assets/js/'
        }
    }
};
